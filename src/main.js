// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import firebase from 'firebase'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueMoment from 'vue-moment'
// import VueFire from 'vuefire'

Vue.use(Vuetify, { iconfont: 'mdi'
})

Vue.use(VueMoment)

// Vue.use(VueFire)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAA6LvhjVLBEkkQasI4GAZSzBfi4xLzd3s',
    libraries: 'places' // necessary for places input
  }
})

/* Vue.use(VueGlobalVariable, {
  globals: {residenciesArray},
}); */

Vue.config.productionTip = false

export const config = {
  apiKey: 'AIzaSyDRvX11w-qNoVZ708uC1fYkfDSEJS5n-mQ',
  authDomain: 'tfmuoc2019.firebaseapp.com',
  databaseURL: 'https://tfmuoc2019.firebaseio.com',
  projectId: 'tfmuoc2019',
  storageBucket: 'tfmuoc2019.appspot.com',
  messagingSenderId: '322091048880',
  appId: '1:322091048880:web:b20caec80da405b1'
}

firebase.initializeApp(config)

export default firebase
export const db = firebase
// localStorage.clear()

/* eslint-disable no-new */
/* new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}) */
let app = ''
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App) }).$mount('#app')
  }
})
