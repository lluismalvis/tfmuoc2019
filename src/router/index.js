import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/content/HelloWorld'
import Home from '@/components/content/Home'
import Signup from '@/components/content/Signup'
import Login from '@/components/content/Login'
import Residencia from '@/components/content/Residencia'
import firebase from 'firebase'
import Starrating from '../components/content/Starrating'
import Comments from '../components/content/Comments'
import Favorits from '../components/content/Favorits'
import Profile from '../components/content/Profile'
import LoginUI from '../components/content/LoginUI'
Vue.use(Router)

// export default new Router({
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: {
        requiresAuth: true}
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: Signup
    },
    {
      path: '/residencia/:id',
      name: 'Residencia',
      component: Residencia,
      // meta: { requiresAuth: true },
      children: [
        {path: '', component: Starrating},
        {path: '', component: Comments}
      ]
    },
    {path: '/favorits',
      name: 'Favorits',
      component: Favorits
    },
    {path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {path: '/loginUI',
      name: 'LoginUI',
      component: LoginUI
    }
  ]

})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!currentUser) next('/login')
    else next()
  } else {
    next()
    this.drawer = true
  }
})

export default router
