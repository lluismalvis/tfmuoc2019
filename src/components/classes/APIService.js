import axios from 'axios/index'
import {Residencia} from '../classes/Residencia'
// import firebase from 'firebase/database'

// var database = firebase.database()

export class APIService {
  constructor () {
    this.urlRestApi = new Array(2)

    var residenciesGeriatriques = []

    var arrayResult = []

    // this.residenciesArray = []

    const urlAvoidCORS = 'https://cors-anywhere.herokuapp.com/'
    this.urlRestApi[0] = 'http://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=1fdd6f57-0f41-4b27-b8f0-57e6d04f229a&limit=500'
    this.urlRestApi[1] = 'http://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=ddaecad1-71b0-4fe4-bbf6-09d09742caf5&limit=500'

    this.urlRestApi.forEach(function (value, key) {
      console.log('value: ' + value + ', key: ' + key)
      axios.get(urlAvoidCORS + value)
        .then(response => {
          // JSON responses are automatically parsed.
          arrayResult = response.data.result.records
          // console.log(response.data.result.records)
          var residenciesArray = []
          // var residenciesArray = new Array(response.data.result.records)
          residenciesArray = arrayResult.slice()

          // AGEFEIX RESIDENCIES LOCALSTORAGE
          var identant = null
          for (let index in residenciesArray) {
            if (identant !== residenciesArray[index].CODI_EQUIPAMENT /* && residenciesArray[index].TELEFON_TIPUS !== 'Fax' */) {
            // if (isExist(identant, residenciesGeriatriques)) {
              var tipusResi = ''
              if (key === 0) { tipusResi = 'residencia' } else { tipusResi = 'centreDeDia' }
              let reside = new Residencia(residenciesArray[index].CODI_EQUIPAMENT, residenciesArray[index].EQUIPAMENT, residenciesArray[index].TIPUS_VIA, residenciesArray[index].NOM_CARRER, residenciesArray[index].NUM_CARRER_1, residenciesArray[index].NUM_CARRER_2, residenciesArray[index].NOM_DISTRICTE, residenciesArray[index].CODI_POSTAL, residenciesArray[index].LATITUD, residenciesArray[index].LONGITUD, residenciesArray[index].TELEFON_NUM, residenciesArray[index].TELEFON_TIPUS, tipusResi, residenciesArray[index].HORARI_HORES_INICI, residenciesArray[index].HORARI_HORES_FI)
              residenciesGeriatriques.push(reside)
              identant = residenciesArray[index].CODI_EQUIPAMENT
            }
          }
          localStorage.setItem('residenciesGeriatriques', JSON.stringify(residenciesGeriatriques))
          // FI AFEGEIX LOCALSTORE
        }).catch(e => {
          // this.errors.push(e)
          console.log(e)
        })
    })

    /* function isExist (value, residenciesGeriatriques) {
      for (var i = 0; i < residenciesGeriatriques.length; i++) {
        if (residenciesGeriatriques[i].CODI_EQUIPAMENT === value) {
          return true
        }
      }
      return false
    } */
  }
  obteTotesResidencies () {
    return JSON.parse(localStorage.getItem('residenciesGeriatriques'))
  }
}
