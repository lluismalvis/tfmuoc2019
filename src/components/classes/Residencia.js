export class Residencia {
  get POSITION () {
    return this._POSITION
  }

  set POSITION (value) {
    this._POSITION = value
  }
  get CODI_EQUIPAMENT () {
    return this._CODI_EQUIPAMENT
  }

  set CODI_EQUIPAMENT (value) {
    this._CODI_EQUIPAMENT = value
  }

  get EQUIPAMENT () {
    return this._EQUIPAMENT
  }

  set EQUIPAMENT (value) {
    this._EQUIPAMENT = value
  }

  get TIPUS_VIA () {
    return this._TIPUS_VIA
  }

  set TIPUS_VIA (value) {
    this._TIPUS_VIA = value
  }

  get NOM_CARRER () {
    return this._NOM_CARRER
  }

  set NOM_CARRER (value) {
    this._NOM_CARRER = value
  }

  get NUM_CARRER_1 () {
    return this._NUM_CARRER_1
  }

  set NUM_CARRER_1 (value) {
    this._NUM_CARRER_1 = value
  }

  get NUM_CARRER_2 () {
    return this._NUM_CARRER_2
  }

  set NUM_CARRER_2 (value) {
    this._NUM_CARRER_2 = value
  }

  get NOM_DISTRICTE () {
    return this._NOM_DISTRICTE
  }

  set NOM_DISTRICTE (value) {
    this._NOM_DISTRICTE = value
  }

  get CODI_POSTAL () {
    return this._CODI_POSTAL
  }

  set CODI_POSTAL (value) {
    this._CODI_POSTAL = value
  }

  get LATITUD () {
    return this._LATITUD
  }

  set LATITUD (value) {
    this._LATITUD = value
  }

  get LONGITUD () {
    return this._LONGITUD
  }

  set LONGITUD (value) {
    this._LONGITUD = value
  }

  get TELEFON_NUM () {
    return this._TELEFON_NUM
  }

  set TELEFON_NUM (value) {
    this._TELEFON_NUM = value
  }

  get TELEFON_TIPUS () {
    return this._TELEFON_TIPUS
  }

  set TELEFON_TIPUS (value) {
    this._TELEFON_TIPUS = value
  }

  get TIPUS_RESIDENCIA () {
    return this._TIPUS_RESIDENCIA
  }

  set TIPUS_RESIDENCIA (value) {
    this._TIPUS_RESIDENCIA = value
  }
  get HORA_INICI () {
    return this._HORA_INICI
  }

  set HORA_INICI (value) {
    this._HORA_INICI = value
  }

  get HORA_FI () {
    return this._HORA_FI
  }

  set HORA_FI (value) {
    this._HORA_FI = value
  }

  constructor (CODI_EQUIPAMENT, EQUIPAMENT, TIPUS_VIA, NOM_CARRER, NUM_CARRER_1, NUM_CARRER_2, NOM_DISTRICTE, CODI_POSTAL, LATITUD, LONGITUD, TELEFON_NUM, TELEFON_TIPUS, TIPUS_RESIDENCIA, HORA_INICI, HORA_FI) {
    this._CODI_EQUIPAMENT = CODI_EQUIPAMENT
    this._EQUIPAMENT = EQUIPAMENT
    this._TIPUS_VIA = TIPUS_VIA
    this._NOM_CARRER = NOM_CARRER
    this._NUM_CARRER_1 = NUM_CARRER_1
    this._NUM_CARRER_2 = NUM_CARRER_2
    this._NOM_DISTRICTE = NOM_DISTRICTE
    this._CODI_POSTAL = CODI_POSTAL
    this._LATITUD = LATITUD
    this._LONGITUD = LONGITUD
    this._TELEFON_NUM = TELEFON_NUM
    this._TELEFON_TIPUS = TELEFON_TIPUS
    this._POSITION = { lat: this.LATITUD, lng: this.LONGITUD }
    this._TIPUS_RESIDENCIA = TIPUS_RESIDENCIA
    this._HORA_INICI = HORA_INICI
    this._HORA_FI = HORA_FI
  }
}
