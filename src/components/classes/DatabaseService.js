
import {db as dbs} from '../../main'

export default class DatabaseService {
  nouUsuari (nom, cognoms, email, avatar) {
    dbs.collection('usuarios').add({
      apellidos: cognoms,
      identificador: email,
      mail: email,
      nombre: nom,
      avatar: avatar

    })
      .then(function () {
        console.log('Document successfully written!')
      })
      .catch(function (error) {
        console.error('Error writing document: ', error)
      })
  }

  getUsuari (id) {
    dbs.collection('usuarios').where('mail', '==', id)
      .then(function () {
        console.log('Document successfully written!')
      })
      .catch(function (error) {
        console.error('Error writing document: ', error)
      })
  }

  nouPost (equipament, usuari, missatge) {
    dbs.collection('posts').add({
      data: new Date(),
      equipament: equipament,
      missatge: missatge,
      usuari_id: usuari
    })
      .then(function () {
        console.log('Document successfully written!')
      })
      .catch(function (error) {
        console.error('Error writing document: ', error)
      })
  }

  afegeixRating (equipament, usuari, rate) {
    dbs.collection('rating').add({
      rate: rate,
      equipament_id: equipament,
      usuari_id: usuari
    })
      .then(function () {
        console.log('Document successfully written!')
      })
      .catch(function (error) {
        console.error('Error writing document: ', error)
      })
  }

  obteRating (equipament) {
    var mitjana = 0
    var count = 0
    dbs.collection('rating').where('equipament_id', '==', equipament).get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, ' => ', doc.data())
          mitjana += doc.data().rate
          count++
        })
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error)
      })
    return mitjana / count
  }

  afegeixFavorits (user, equipament) {
    dbs.collection('favorits').add({
      data: new Date(),
      equipament_id: equipament,
      usuari_id: user
    })
  }
}
